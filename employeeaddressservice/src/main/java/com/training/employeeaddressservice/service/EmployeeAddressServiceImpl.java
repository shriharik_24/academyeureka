package com.training.employeeaddressservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.training.employeeaddressservice.model.Address;
import com.training.employeeaddressservice.model.Employee;

@Service
public class EmployeeAddressServiceImpl implements EmployeeAddressService {

	@Autowired
	EmployeeWebService empWebSevice;

	@Autowired
	AddressWebService addressWebService;

	@HystrixCommand(fallbackMethod = "defaultEmployee")
	public List<Employee> getEmployees() {
		List<Employee> empList = empWebSevice.getEmployees();
		System.out.println();
		
		for (Employee employee : empList) {
			employee.setAddress(addressWebService.getAddressByEmployeeId(employee.getId()));
		}
		return empList;
	}
	
	
	public List<Employee> defaultEmployee(){
		
		List<Employee> emp = new ArrayList<Employee>();
		
		emp.add(Employee.builder().id(1).empName("Rosaline").salary(-1).address(Address.builder().state("TN").street("HCL Street").city("CN").country("IN").id(-1).employeeId(-1).pincode("-1").build()).build());
		return emp;
	}
	
	
	
//	@HystrixCommand(fallbackMethod = "defaultAddress")
//	public Address getAddress(Employee employee) {
//		return addressWebService.getAddressByEmployeeId(employee.getId());
//	}
	
//	public Address defaultAddress(Employee employee) {
//		return Address.builder().state("TN").street("HCL Street").city("CN").country("IN").id(-1).employeeId(-1).pincode("-1").build();
//	}

	public Employee addEmployeeAddress(Employee employee) {
		employee.setAddress(addressWebService.addEmployee(employee.getAddress()));
		return employee;
	}

}
