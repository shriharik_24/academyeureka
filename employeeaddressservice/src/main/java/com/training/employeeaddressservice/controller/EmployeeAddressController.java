package com.training.employeeaddressservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.training.employeeaddressservice.model.Address;
import com.training.employeeaddressservice.model.Employee;
import com.training.employeeaddressservice.service.EmployeeAddressService;

@RestController
public class EmployeeAddressController {
	
	@Autowired
	EmployeeAddressService empAddressService;
	
	//@HystrixCommand(fallbackMethod = "defaultEmployee")
	@GetMapping("/employees")
	public List<Employee> getEmployees(){
		return empAddressService.getEmployees();
	}
	
	public List<Employee> defaultEmployee(){
		System.out.println("something went wrong");
		List<Employee> emp = new ArrayList<Employee>();
		
		emp.add(Employee.builder().id(1).empName("Daniel").salary(1234).address(Address.builder().state("TN").street("HCL Street").city("CN").country("IN").id(-1).employeeId(-1).pincode("-1").build()).build());
		return emp;
	}
	
	@PostMapping("/address")
	public Employee createAddress(@RequestBody Employee employee) {
		return empAddressService.addEmployeeAddress(employee);
	}
	
}
