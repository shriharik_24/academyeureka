package com.training.courseservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.courseservice.model.Course;
import com.training.courseservice.service.CourseService;

@RestController
public class CourseController {

	@Autowired
	CourseService courseService;

	@GetMapping(value = "/courses")
	public List<Course> getAllCourses() {
		return courseService.getCourses();
	}

	@GetMapping(value = "/course")
	public Course getOneCourse(@RequestParam long id) {
		return null;// courseService.getOneCourse(id);
	}

	@GetMapping(value = "/course/{courseId}")
	public Course getOneCourseById(@PathVariable long courseId) {
		return null; // courseService.getOneCourse(courseId);
	}

	@PostMapping(value = "/course")
	public ResponseEntity createCourse(@RequestBody Course course) {
		return new ResponseEntity<>(courseService.createCourse(course), HttpStatus.CREATED);
	}

	@PutMapping(value = "/course")
	public Course updateCourse(@RequestBody Course course) {
		System.out.println("Update the course:" + course);
		return null;// courseService.updateCourse(course);
	}

	@DeleteMapping(value = "/course/{courseId}")
	public String deleteCourseById(@PathVariable long courseId) {
		try {
			if (true) { // courseService.deleteCourse(courseId)
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Cannot able to delete the course";

	}
}
