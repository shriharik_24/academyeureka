package com.training.courseservice.service;

import java.util.List;

import com.training.courseservice.model.Course;

public interface CourseService {
	public List<Course> getCourses();
	public Course createCourse(Course course);
}
