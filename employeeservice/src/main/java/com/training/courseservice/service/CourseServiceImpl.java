package com.training.courseservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.courseservice.model.Course;
import com.training.courseservice.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	CourseRepository courseRepository;
	
	@Override
	public List<Course> getCourses() {
		return courseRepository.findAll();
	}

	@Override
	public Course createCourse(Course course) {
		return courseRepository.save(course);
	}

}
