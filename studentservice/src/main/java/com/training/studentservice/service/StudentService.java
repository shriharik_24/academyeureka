package com.training.studentservice.service;

import java.util.List;

import com.training.studentservice.model.Student;

public interface StudentService {
	public List<Student> getStudents();
	public Student createStudent(Student student);
	public Student getStudentByCourseId(long courseId);
}
