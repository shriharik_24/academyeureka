package com.training.studentservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.studentservice.model.Student;
import com.training.studentservice.service.StudentService;

@RestController
public class StudentController {

	@Autowired
	StudentService studentService;

	@GetMapping(value = "/student/course/{courseId}")
	// http://localhost:1234/address/2
	// do not give too many path variables
	public Student getStudentByCourseId(@PathVariable long courseId) {
		System.out.println("I am invoked for courseId:" + courseId);
		return studentService.getStudentByCourseId(courseId);
	}

	@PostMapping(value = "/student")
	public ResponseEntity createStudent(@RequestBody Student student) {
		System.out.println(student);
		return new ResponseEntity<>(studentService.createStudent(student), HttpStatus.CREATED);
	}
	
	

}
